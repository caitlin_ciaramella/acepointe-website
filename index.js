require('dotenv').config()
const express = require('express');
const app = express();
const http = require('http');
const path = require('path');
const cors = require('cors');

require("es6-promise").polyfill();
require("isomorphic-fetch");

app.use(express.static(path.join(__dirname, '/build')))


const {RECAPTCHA_SECRET_KEY} = process.env

app.use(express.json());
app.use(cors());

app.post(`/api/verify`, async (req, res) => {
    const {captchaValue} = req.body
    console.log(captchaValue)

const isHuman = await fetch(`https://www.google.com/recaptcha/api/siteverify`, {
  method: "post",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
  },
  body: `secret=${RECAPTCHA_SECRET_KEY}&response=${captchaValue}`
})
  .then(res => res.json())
  .then(json => json.success)
  
  .catch(err => {
    throw new Error(`Error in Google Siteverify API. ${err.message}`)
  })

if (captchaValue === null || !isHuman) {
  res.status(404).send('did not work')
  throw new Error(`YOU ARE NOT A HUMAN.`)
}

    console.log('SUCCESS!')
    res.status(200).send('OK')
})

app.get('/*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'build', 'index.html'));
  res.status(200);
});

const server = http.createServer(app);

const port = process.env.PORT || 1337;

server.listen(port);

console.log("Server running at http://localhost:%d", port);