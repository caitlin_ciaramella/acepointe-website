import React from 'react';
import './ProjectPopUp.css';
import {GrClose} from 'react-icons/gr';
import parse from 'html-react-parser';


function ProjectPopUp(props){

    return (
        <div className='project-pop-up-background'>
        <div className='project-pop-up-container'>
            <div className='close-btn-section'>
                < GrClose onClick={() => {props.updateProj(null); props.updateInterval(5000)}} className='pop-up-close-btn'/>
            </div>

            <div className='project-pop-up-section'>
                <div className='popup-company-title'>
                {props.project.Company}
                </div> 
                {/* <div className='popup-project-image'> */}
                    <img src={props.project.ProjectImage} alt={props.project.Company} className='popup-project-image'/>
                {/* </div> */}
                <div className='popup-project-description'>
                    {parse(props.project.ProjectDesription)}
                </div>
            </div>

        </div>
        </div>
    )
}

export default ProjectPopUp