import React from "react";
import Carousel from 'react-bootstrap/Carousel';
import './Carousel.css';
import parse from 'html-react-parser';
import {FiPlus} from 'react-icons/fi';
import { FlagSpinner } from "react-spinners-kit"




function ProjectCarousel(props) {
  console.log(props.dataArray)
  if(props.dataArray == null){
    return (
      <div style={{paddingTop: '100px'}}><FlagSpinner size={90} color="#686769" loading={true} /></div>
    )
  }
    if(props.dataArray !== null){
      return(
      <Carousel className='carousel' interval={props.interval}>
      {props.dataArray.map(project => {
        return (
          
          <Carousel.Item key={project.id} className='carousel-item'>
            <div className='project-card' >

              <img src={project.PreviewImage} alt={project.Company} className=
              {project.id === 7 ? 'eklipse' : project.id === 8 ? 'equiant' : 
                project.id === 9 ? 'hookes' : project.id === 10 ? 'megalab' : ''}
              />
              
              <div className='project-info'>
                
                <div className='project-summary'>
                  <p className='project-header'>
                    {parse(project.PreviewHeader)}
                  </p>
                  <p className='project-content'>
                    {parse(project.PreviewSummary)} 
                    <FiPlus className='more-icon-services' onClick={() => {props.updateProj(project); props.updateInterval(null)}}/>
                  </p>
                </div>

              </div>

            </div>
            
          </Carousel.Item>
          
        )
      })}

     </Carousel>
      )
    } else {
    return (
        ''
      )
    }
  
}


export default ProjectCarousel;