import React, {useState, useEffect} from 'react';
import './Projects.css'
import BottomNav from '../PageFeatures/BottomNav';
import ProjectCarousel from './Carousel';
import ProjectPopUp from './ProjectPopUp';
import axios from 'axios';


function Projects() {
    const [openProj, setOpenProj] = useState(null);
    const [interval, setInterval] = useState(8000);
    const [projectData, setProjectData] = useState(null)

    const updateInterval = (currInterval) => {
        setInterval(currInterval)
    }

    const updateProj = (currProj) => {
        setOpenProj(currProj)
        console.log('updating state')
    }


    useEffect(() => {
        getData()
    }, [])


    const getData = () => {
        console.log('pre request')
        axios.get('https://acepointeapi.azurewebsites.net/getprojectdetails')
        .then(res => {
            let data = JSON.parse(res.data)
            console.log('data', data)
                setProjectData(data)
        })
        .catch(err => console.log(err))
    }

    return (
        <div className='projects-container' id='projects'>
            <div className='projects-title'>
               Some Projects We're Proud Of
            </div>
            <ProjectCarousel dataArray={projectData} updateProj={updateProj} updateInterval={updateInterval} interval={interval}/>

            {openProj ? <ProjectPopUp project={openProj} updateProj={updateProj} updateInterval={updateInterval}/> : ''}

            <BottomNav name={'projects'} url={'#clients'} page={'OUR CLIENTS'} />
        </div>
    )
}

export default Projects;