import React from 'react';
import './Clients.css'
import ClientImages from './ClientLogos.png'
import BottomNav from '../PageFeatures/BottomNav'


function Clients() {
    return (
        <div className='clients-container' id='clients'>
            <div className='clients-title'>
                Some friends we've made along the way
            </div>
            <div className='clients-section'>
                <img className='client-logo-img' src={ClientImages} alt='client logo'></img>
            </div>
            <BottomNav name={'clients'} url={'#contact'} page={'CONTACT US'} />
        </div>
    )
    
}

export default Clients;