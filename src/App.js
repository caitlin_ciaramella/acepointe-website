import React  from 'react';
import './App.css';
import AcePointe from './AcePointe'
import {Switch, Route} from 'react-router-dom';




function App() {


  return (
    <div>
      <Switch>
        <Route path='/' component={AcePointe} />
      </Switch>
    </div>
  );
}

export default App;
