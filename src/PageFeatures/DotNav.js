import React, {useState} from 'react';
import './DotNav.css';
import {HashLink as Link} from 'react-router-hash-link';
// import {useHistory} from 'react-router-dom';
import { FaRegCircle } from 'react-icons/fa';
import { FaCircle } from 'react-icons/fa';


function DotNav(props) {
    const [homeHover, setHomeHover] = useState(false)
    const [aboutHover, setAboutHover] = useState(false)
    const [servicesHover, setServicesHover] = useState(false)
    const [projectsHover, setProjectsHover] = useState(false)
    const [clientsHover, setClientsHover] = useState(false)
    const [contactHover, setContactHover] = useState(false)
    const toggleHome = () => setHomeHover(!homeHover)
    const toggleAbout = () => setAboutHover(!aboutHover)
    const toggleServices = () => setServicesHover(!servicesHover)
    const toggleProjects = () => setProjectsHover(!projectsHover)
    const toggleClients = () => setClientsHover(!clientsHover)
    const toggleContact = () => setContactHover(!contactHover)

    return (
        <div className='dot-nav-container'>
            <ul>
                <li>
                    <Link to='/#home' className='dot-nav-icon' onMouseEnter={toggleHome}  onMouseLeave={toggleHome}> 
                    {props.currIdx === 0 || props.currIdx === null ? <FaCircle className='circle-icon'/> : < FaRegCircle className='circle-icon'/>}
                    {props.currIdx === 0 ? <span className='dot-nav-label'>Home</span> : ''}
                    {homeHover && props.currIdx !== 0 ? <span className='dot-nav-label'>Home</span> : ''}
                    </Link>
                </li>
                <li>
                    <Link to='/#about' className='dot-nav-icon' onMouseEnter={toggleAbout} onMouseLeave={toggleAbout}> 
                    {props.currIdx === 1 ? <FaCircle className='circle-icon'/> : < FaRegCircle className='circle-icon'/>}
                    {props.currIdx === 1 ? <span className='dot-nav-label'>About Us</span> : ''}
                    {aboutHover && props.currIdx !== 1 ? <span className='dot-nav-label'>About Us</span> : ''}
                    </Link>
                </li>
                <li>
                    <Link to='/#services' className='dot-nav-icon' onMouseEnter={toggleServices} onMouseLeave={toggleServices}> 
                    {props.currIdx === 2 ? <FaCircle className='circle-icon'/> : < FaRegCircle className='circle-icon'/>}
                    {props.currIdx === 2 ? <span className='dot-nav-label'>Services</span> : ''}
                    {servicesHover && props.currIdx !== 2  ? <span className='dot-nav-label'>Services</span> : ''}
                    </Link>
                </li>
                <li>
                    <Link to='/#projects' className='dot-nav-icon' onMouseEnter={toggleProjects} onMouseLeave={toggleProjects}> 
                    {props.currIdx === 3 ? <FaCircle className='circle-icon'/> : < FaRegCircle className='circle-icon'/>}
                    {props.currIdx === 3 ? <span className='dot-nav-label'>Projects</span> : ''}
                    {projectsHover && props.currIdx !== 3 ? <span className='dot-nav-label'>Projects</span> : ''}
                    </Link>
                </li>
                <li>
                    <Link to='/#clients' className='dot-nav-icon' onMouseEnter={toggleClients} onMouseLeave={toggleClients}> 
                    {props.currIdx === 4 ? <FaCircle className='circle-icon'/> : < FaRegCircle className='circle-icon'/>}
                    {props.currIdx === 4 ? <span className='dot-nav-label'>Our Clients</span> : ''}
                    {clientsHover && props.currIdx !== 4 ? <span className='dot-nav-label'>Our Clients</span> : ''}
                    </Link>
                </li>
                <li>
                    <Link to='/#contact' className='dot-nav-icon' onMouseEnter={toggleContact} onMouseLeave={toggleContact}> 
                    {props.currIdx === 5 ? <FaCircle className='circle-icon'/> : < FaRegCircle className='circle-icon'/>}
                    {props.currIdx === 5 ? <span className='dot-nav-label'>Contact Us</span> : ''}
                    {contactHover && props.currIdx !== 5 ? <span className='dot-nav-label'>Contact Us</span> : ''}
                    </Link>
                </li>
            </ul>
        </div>
    )
}


export default DotNav;