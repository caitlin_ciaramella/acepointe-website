import React from 'react';
import './Footer.css';
import { FaPhoneAlt } from 'react-icons/fa';
import { MdEmail } from 'react-icons/md';
import { MdCopyright } from 'react-icons/md';



function Footer() {
    return (
        <footer className='footer'>
            <span className='contact-info'> 
                <span className='footer-icon'> < FaPhoneAlt /> </span>  480 597 5205 
                &ensp; 
                <span className='footer-icon'> < MdEmail /> </span> info@acepointe.com 
            </span> 
            <span> 
                <span className='footer-icon'> < MdCopyright /> </span> 2020 Developed by AcePointe 
            </span>
        </footer> 
    )
}

export default Footer;
