import React from 'react';
import './SocialMedia.css';
import { FaFacebookF } from 'react-icons/fa';
import { FaTwitter } from 'react-icons/fa';
import { GrLinkedin } from 'react-icons/gr';


function Media(props) {
    return (
        <div className='social-media-container'>
            <ul>
                <li>
                    <a  className= 'social-media-icon' href='https://www.facebook.com/AcePointe-100980914994319'> 
                    < FaFacebookF /> 
                    </a>
                </li>
                <li>
                    <a className= 'social-media-icon' href='https://twitter.com/AcePointe'> 
                    < FaTwitter /> 
                    </a>
                </li>
                <li>
                    <a className= 'social-media-icon' href='https://www.linkedin.com/company/acepointe/'> 
                    < GrLinkedin /> 
                    </a>
                </li>
            </ul>
        </div>
    )
}


export default Media;