import React, {useState} from 'react';
import './Header.css';
import {HashLink as Link} from 'react-router-hash-link';
import logo from './AceLogo-red.png';
import { MdMenu } from 'react-icons/md';
import { MdClear } from 'react-icons/md';


//change the <a> href anchor tag 
function AcePointeLogo() {
    return (
        <div className='acepointe-logo-container'>
            <a href='#home'><img className='acepointe-img' src={logo} alt='AcePointe logo'/></a>
        </div>
    )
}

function Menu(props) {
    return (
            <div className='pop-out-menu'>
                <nav>
                    <ul className='pop-out-menu-items'>
                        <li><Link onClick={props.onMenuClick} to='#home' className='pop-out-menu-items-link'>HOME</Link></li>
                        <li><Link onClick={props.onMenuClick} to='#about' className='pop-out-menu-items-link'>ABOUT US</Link></li>
                        <li><Link onClick={props.onMenuClick} to='#services' className='pop-out-menu-items-link'>SERVICES</Link></li>
                        <li><Link onClick={props.onMenuClick} to='#projects' className='pop-out-menu-items-link'>PROJECTS</Link></li>
                        <li><Link onClick={props.onMenuClick} to='#clients' className='pop-out-menu-items-link'>OUR CLIENTS</Link></li>
                        <li><Link onClick={props.onMenuClick} to='#contact' className='pop-out-menu-items-link'>CONTACT US</Link></li>
                    </ul>
                </nav>
            </div> 
    )
}

function MenuBtn(props) {
    return (
        <button id='menu-btn' onClick={props.onMenuClick}>
            {props.open === true ? 
            <MdClear className='react-icon mobile-close'/> 
            : 
            <MdMenu className='react-icon'/>}
        </button>
    )
}

function Header() {
    const [open, setOpen] = useState(false);
    const onMenuClick = () => {
        setOpen(!open);
    }

    return (
        <header>
            <AcePointeLogo />
            <MenuBtn open={open} onMenuClick={onMenuClick} />
            {open ? <Menu onMenuClick={onMenuClick}/> : ''}
        </header>
    )
}


export default Header;