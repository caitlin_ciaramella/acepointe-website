import React from 'react';
import './BottomNav.css';
import {HashLink as Link} from 'react-router-hash-link';


function BottomNav(props){
    return (
            <div className={`page-${props.name} bottom-container`}>
                <Link to={props.url} className='bottom-nav-link'>
                    <div className='line-div'></div>
                    {props.page}
                </Link>
            </div>
    )
}

export default BottomNav;

    
