import React, {useState} from 'react';
import './Services.css'
import BottomNav from '../PageFeatures/BottomNav';
import {GrClose} from 'react-icons/gr';
import {FiPlus} from 'react-icons/fi';
import {FiFigma} from 'react-icons/fi';
import adobe from './adobexd.png';
import invision from './invision.png';
import {GrHtml5} from 'react-icons/gr';
import {IoLogoCss3} from 'react-icons/io';
import {IoLogoJavascript} from 'react-icons/io';
import {FaReact} from 'react-icons/fa';
import xamarin from './xamarin.png';
import mongo from './mongo.png';
import python from './python.png';
import tableau from './tableau.png';
import powerbi from './powerbi.png';
import azuresynapse from './azuresynapse.png';
import azuredatalake from './azuredatalake.png';
import thoughtspot from './thoughtspot.png';
import {FaAws} from 'react-icons/fa';
import {DiGoogleCloudPlatform} from 'react-icons/di';
import {DiScrum} from 'react-icons/di';
import azure from './azure.png';
import atlassian from './atlassian.png';



function Services() {
    const [open, setOpen] = useState(null)

    return (
        <div className='services-container' id='services'>

            <div className='services-sections'>

                <div className='services-title-container'>
                    <div className='services-title'>
                        What we do best.
                    </div> 
                    <div className='services-subtitle'> 
                    We partner with your team to deliver full end-to-end 
                    digital products, solutions and consulting services.
                    </div>
                </div>

                <div className='service-item-grid'>
                    <div className='service-item'>
                        <div className='service-item-title'>
                            Web & App Development 
                        </div>
                        <div className='service-item-desc'>
                            Using the latest technology we provide high-quality web design and application 
                            development services that deliver exceptional designs and functionality that 
                            makes you stand out among the crowd while staying true to the spirit of your business.
                            <div className='more-icon-container'onClick={() => setOpen('web/app')}><FiPlus className='more-icon-services'/></div>
                        </div>
                        
                    </div>

                    <div className='service-item'>
                        <div className='service-item-title'>
                            UX/UI Design 
                        </div>
                        <div className='service-item-desc'>
                            We believe a powerful user interface is key to delivering unmatched user 
                            experience. It's more than sophisticated code and pretty pages. This is why we work 
                            closely with our clients and to first understand your brand, and then design and develop cutting-edge 
                            UX/UI designs that bring all the pieces together.
                            <div className='more-icon-container' onClick={() => setOpen('ux/ui')}><FiPlus className='more-icon-services'/></div>
                        </div>
                    </div>

                    <div className='service-item'>
                        <div className='service-item-title'>
                            Data Analytics 
                        </div>
                        <div className='service-item-desc'>
                            Imagine having a team of data analytics consultants and data experts partnering 
                            with you to uncover the power of your data and all the potential within it. 
                            Whether it’s how to analyze critical business data, move analytics to the Cloud, 
                            or automation, we offer a diverse suite of analytics solutions that are designed 
                            to meet your data analytics needs.
                            <div className='more-icon-container' onClick={() => setOpen('data')}><FiPlus className='more-icon-services'/></div>
                        </div>
                    </div>

                    <div className='service-item'>
                        <div className='service-item-title'>
                            Consulting 
                        </div>
                        <div className='service-item-desc'>
                            Make your brand the leader by talking with our in-house experts. Our technology 
                            consulting services guide you through your initiative by leveraging latest technology, 
                            well thought out strategies and design thinking approaches for your journey, whatever 
                            it may be, and wherever you are in the process. Let our years of experience go to 
                            work for you!
                            <div className='more-icon-container' onClick={() => setOpen('consulting')}><FiPlus className='more-icon-services'/></div>
                        </div>
                    </div>
                </div>
                <BottomNav name={'services'} url={'#projects'} page={'PROJECTS'} />
            </div>

            {open === 'ux/ui' ?
                <div className='services-pop-up-background'>
                    <div className='services-pop-up'>
                        <div className='services-pop-up-section'>
                            <GrClose onClick={() => setOpen(null)} className='services-close-icon'/>
                            <div>
                                <div className='services-pop-up-title'>
                                    UX/UI Design
                                </div>
                                <div className='services-pop-up-subtitle'>
                                    How do we create that exceptional online experience? 
                                    <br/>
                                    How do we engage your site visitors to come back for more and refer others to you? 
                                </div>
                                <div className='services-more-info-text'>  
                                    Our commitment to collaborating with our client is key. From project concept to actualization, 
                                    we partner with you to leverage the know-how of your business and our technology know-how we 
                                    create the perfect outcome, which users deeply connect with. 
                                    <div className='tech-images-section'>
                                        <FiFigma className='tech-icon'/>
                                        <img src={adobe} alt='adobe xd' className='tech-img'/>
                                        <img src={invision} alt='invision' className='tech-img'/>
                                    </div>
                                    Throughout the process, we work to gain important insights using methodologies like journey mapping, experience mapping, usability 
                                    testing, benchmarking, service blueprints, ideation and concept design, user flow analysis etc., 
                                    and the results of these become the basis for approaches taken and technical decisions made during 
                                    design and development of your project.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                : ''
            }

            {open === 'web/app' ?
                <div className='services-pop-up-background'>
                    <div className='services-pop-up'>
                        <div className='services-pop-up-section'>
                            <GrClose onClick={() => setOpen(null)} className='services-close-icon'/>
                            <div>
                                <div className='services-pop-up-title'>
                                   Web & App Development
                                </div>
                                <div className='services-pop-up-subtitle'>
                                    Fast-moving trends in web and app development require rock solid technical expertise. 
                                </div>
                                <div className='services-more-info-text'>  
                                    At AcePointe, we love to turn your ideas into 
                                    apps, and to stay ahead of the curve, our team is constantly learning about the latest 
                                    front-end and back-end development frameworks or stacks so that we’re able to deliver modern, 
                                    easy to use, and functional websites and apps for IOS, Android and Windows. 
                                <div className='tech-images-section'>
                                        <GrHtml5 className='tech-icon'/>
                                        <IoLogoCss3 className='tech-icon'/>
                                        <IoLogoJavascript className='tech-icon'/>
                                        <FaReact className='tech-icon'/>
                                        <img src={xamarin} alt='xamarin' className='tech-img'/>
                                </div>
                                    Our deep development 
                                    passion and expertise paired with our agile approach to development is how we create top class websites, 
                                    mobile apps and outstanding brand identities. We strong believers in having our client as part of the 
                                    project team, keeping you updated, and leveraging your business knowledge to ensure the final product 
                                    meets your needs and more!
                                </div>
                           </div>
                        </div>
                    </div>
                </div>
                : ''
            }

            {open === 'data' ?
                <div className='services-pop-up-background'>
                    <div className='services-pop-up'>
                        <div className='services-pop-up-section'>
                            <GrClose onClick={() => setOpen(null)} className='services-close-icon'/>
                            <div>
                                <div className='services-pop-up-title'>
                                   Data Analytics
                                </div>
                                <div className='services-pop-up-subtitle'>
                                    Data is one of a company's most precious assets. 
                                </div>
                                <div className='services-more-info-text'>  
                                    Here at AcePointe, we offer a portfolio of analytics services including data warehousing, self-service BI, 
                                    data visualization and reporting, big data as well as customized analytical 
                                    solutions that meet your needs. 
                                <div className='tech-images-section'>
                                    <img src={mongo} alt='mongo' className='tech-img'/>
                                    <img src={python} alt='python' className='tech-img'/>
                                    <img src={tableau} alt='tableau' className='tech-img'/>
                                    <img src={powerbi} alt='power bi' className='tech-img'/>
                                    <img src={azuresynapse} alt='azure synapse' className='tech-img'/>
                                </div>
                                <div className='tech-images-section2'>
                                    <img src={azuredatalake} alt='azure data lake' className='tech-img'/>
                                    <img src={thoughtspot} alt='thought spot' className='tech-img'/>
                                </div>
                                    Whether you want to make data-driven decisions 
                                    quicker, tap into progressive insights, reduce operational costs, enhance 
                                    productivity, or explore growth opportunities, our multifunctional team with 
                                    many years of experience in the BI and analytics industry can help you reach 
                                    your goals.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                : ''
            }

            {open === 'consulting' ?
                <div className='services-pop-up-background'>
                    <div className='services-pop-up'>
                        <div className='services-pop-up-section'>
                            <GrClose onClick={() => setOpen(null)} className='services-close-icon'/>
                            <div>
                                <div className='services-pop-up-title'>
                                   Consulting
                                </div>
                                <div className='services-pop-up-subtitle'>
                                    There’s untapped intelligence behind your data, we can help you unlock it.
                                </div>
                                <div className='services-more-info-text'>  
                                    Big data, Realtime, multi cloud strategy, in-memory data, natural language processing, 
                                    artificial intelligence, predictive and prescriptive analysis etc have become BI 
                                    and analytics trends and jargons which companies seeking to gain competive advantage 
                                    have to decipher as they navigate the analytics world. 
                                <div className='tech-images-section'>
                                    <img src={atlassian} alt='atlassian' className='tech-img'/>
                                    <DiGoogleCloudPlatform className='tech-icon'/>
                                    <DiScrum className='tech-icon'/>
                                    <img src={azure} alt='azure' className='tech-img'/>
                                    <FaAws className='tech-icon'/>
                                </div>
                                    The good news is, you don’t have to figure it out alone. As your consulting service 
                                    partner, we work with you to develop systems that leverage the power of your data, 
                                    by first understanding your business model and objectives, then selecting the right 
                                    tools, platforms and technologies that support their implementation. 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                : ''
            }    
            

        </div>
    )
}

export default Services;