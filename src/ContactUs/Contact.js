import React, {useState} from 'react';
import './Contact.css';
import { IoIosMail } from 'react-icons/io';
import axios from 'axios';
import ReCAPTCHA from 'react-google-recaptcha';
import CircularProgress from '@material-ui/core/CircularProgress'


function Contact() {
    const [FirstName, setFirstName] = useState('');
    const [LastName, setLastName] = useState('');
    const [Email, setEmail] = useState('');
    const [Message, setMessage] = useState('');
    const [verify, setVerify] = useState(false);
    const [errFName, setErrFName] = useState(null);
    const [errLName, setErrLName] = useState(null);
    const [emailErr, setEmailErr] = useState(null);
    const [errMsg, setErrMsg] = useState(null);
    const [verifyErr, setVerifyErr] = useState(null);
    const [loading, setLoading] = useState(false);

    function submitVerifyForm(e){
        e.preventDefault(e)
        checkFirstName(); 
        checkLastName(); 
        checkEmail(); 
        checkMessage();
        checkHuman();
        postForm();
    }

    function checkFirstName() {
        if(!FirstName){
            setErrFName(true)
        }
    }

    function checkLastName() {
        if(!LastName){
            setErrLName(true)
        } 
    }

    function checkEmail(){
        if(!(Email.includes('@'))){
            setEmailErr(true)
        } 
    }

    function checkMessage(){
        if(!Message){
            setErrMsg(true)
        } 
    }

    function checkHuman(){
         if(!verify){
            setVerifyErr(true)
        }
    }

    function onCaptchaChange(captchaValue) {
        console.log("Captcha value: ", captchaValue)
        axios.post(`/api/verify`, {captchaValue})
        .then( res => {
            console.log('res', res)
            if(res.status === 200){
                setVerify(true)
                setVerifyErr(false)
            }
            else{
                alert('We were unable to verify you are human.')
            }
            
        })
        .catch(err => {
            console.log(err)
        })
    }

    let captcha;

    function postForm(){
        console.log('hit postform()')
        if(errFName === false && errLName === false && emailErr === false && errMsg === false && verifyErr === false){
            setLoading(true)
            axios.post('http://acepointeapi.azurewebsites.net/SendContactEmail', {FirstName, LastName, Email, Message})
            .then(res =>{
                console.log('sent post')
                console.log(res)
                setLoading(false)
                clearInput()
                alert('AcePointe received your message!')
            })
            .catch(err => {
                console.log(err)
            })
        } else {
            console.log('did not do post form')
        }
    }

    function clearInput(){
        setFirstName('')
        setLastName('')
        setEmail('')
        setMessage('')
        setErrFName(null)
        setErrLName(null)
        setEmailErr(null)
        setErrMsg(null)
    }



    return (
        <div className='contact-container' id='contact'>
            <div className='contact-us-section'>

                <div className='contact-title'>
                    Get In Touch
                </div>
                <div className='contact-us-subtitle'>
                    Let's create something better together, we're here for you.
                </div>


                <form className='contact-form-container'>
                    <div className='name-input-container'>
                        <div className='name name-div'>
                            <input onChange={(e) => {setFirstName(e.target.value); setErrFName(false)}} value={FirstName} className='input-box' type='text' placeholder='First Name*' required></input>
                            {errFName ? <span className='err-msg inline'> required </span> : ''}
                        </div>
                        <div className='name-div'>
                        <input onChange={(e) => {setLastName(e.target.value); setErrLName(false)}}  value={LastName} className='input-box' type='text' placeholder='Last Name*' required></input>
                        {errLName ? <span className='err-msg inline'> required</span> : ''}
                        </div>
                    </div>
                     
                    <div>
                        <input onChange={(e) => {setEmail(e.target.value); setEmailErr(false)}} value={Email} className='input-box top-marg' type='email' placeholder='Email*' required></input>
                        {emailErr ? <span className='err-msg'> please enter a valid email </span> : ''}
                    </div>

                    <textarea onChange={(e) => {setMessage(e.target.value); setErrMsg(false)}} value={Message}  className='input-body-box top-marg' placeholder='Message*' required></textarea>
                    {errMsg ? <span className='err-msg'> message required </span> : ''}

                    <ReCAPTCHA style={{marginTop: '10px', transform: 'scale(0.8)', transformOrigin: 0}} sitekey="6LeBS6gZAAAAADcl2w47q1aBWP9mKNumks1SFP0d" onChange={onCaptchaChange} />
                    {verifyErr ? <div className='err-msg'> verify you are human </div> : ''}

                    <div className='send-btn-container' >
                        <button 
                        onClick = {(e) => {submitVerifyForm(e); checkEmail()}}
                        className='send-btn'>
                            {
                            loading ? 
                            <span><CircularProgress disableShrink size={28} style={{color: "white"}}/></span> :
                            <span >< IoIosMail className='mail-icon'/>SEND</span> 
                            }
                        </button>
                    </div>
                </form>

            </div>

        </div>
    )
}

export default Contact;