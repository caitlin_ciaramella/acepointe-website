import React, {useEffect, useState} from 'react';
import './AcePointe.css'
import Header from './PageFeatures/Header';
import Home from './Home/Home';
import About from './AboutUs/About';
import Services from './Services/Services';
import Projects from './Projects/Projects';
import Clients from './Clients/Clients';
import Contact from './ContactUs/Contact';
import Media from './PageFeatures/SocialMedia';
import Footer from './PageFeatures/Footer';
import DotNav from './PageFeatures/DotNav';
import { useHistory } from 'react-router';
import Particles from "react-tsparticles";



function AcePointe() {

  const particlesInit = (main) => {
    console.log(main);

    // you can initialize the tsParticles instance (main) here, adding custom shapes or presets
  };

  const particlesLoaded = (container) => {
    console.log(container);
  };

  const history = useHistory(Math.floor(window.pageYOffset / window.innerHeight))

  const [currIdx, setCurrIdx] = useState(0);
  

  const pages = ['home', 'about', 'services', 'projects', 'clients', 'contact']


  let onScrollHandler = () => {

    let windowCalc = window.pageYOffset / window.innerHeight

   
      if((windowCalc) - Math.floor(windowCalc) === 0){
        
        console.log('index is: ', Math.floor(windowCalc))
        history.replace(`/#${pages[Math.floor(windowCalc)]}`)
        //tried history.replace -> does not keep browser history on scroll 
        // history.push(`/#${pages[Math.floor(windowCalc)]}`) this is the original
        setCurrIdx(Math.floor(windowCalc))
      }
  }

  useEffect(() => {
    window.addEventListener('scroll', onScrollHandler)
  }, [])



  return (
    <div>
      <Header />
    
      <Media />
      <Home />
      <About />
      <Services />
      <Projects />
      <Clients />
      <Contact />
      <Footer />
      <Particles
      id="tsparticles"
      init={particlesInit}
      loaded={particlesLoaded}
      options={{
        background: {
          color: {
            value: "transparent",
          },
        },
        fpsLimit: 60,
        interactivity: {
          events: {
            onClick: {
              enable: true,
              mode: "push",
            },
            onHover: {
              enable: true,
              mode: "repulse",
            },
            resize: true,
          },
          modes: {
            bubble: {
              distance: 400,
              duration: 2,
              opacity: 0.8,
              size: 40,
            },
            push: {
              quantity: 4,
            },
            repulse: {
              distance: 200,
              duration: 0.4,
            },
          },
        },
        particles: {
          color: {
            value: "#ffffff",
          },
          links: {
            color: "#ffffff",
            distance: 150,
            enable: true,
            opacity: 0.5,
            width: 1,
          },
          collisions: {
            enable: true,
          },
          move: {
            direction: "none",
            enable: true,
            outMode: "bounce",
            random: false,
            speed: 6,
            straight: false,
          },
          number: {
            density: {
              enable: true,
              value_area: 800,
            },
            value: 80,
          },
          opacity: {
            value: 0.5,
          },
          shape: {
            type: "circle",
          },
          size: {
            random: true,
            value: 5,
          },
        },
        detectRetina: true,
      }}
    />
      <DotNav currIdx={currIdx} />
    </div>
  );
}


export default AcePointe;

