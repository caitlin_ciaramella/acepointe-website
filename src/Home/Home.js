import React from 'react';
import './Home.css';
import BottomNav from '../PageFeatures/BottomNav';


function Home() {
    return (
        <div className='home-container' id='home'>
            <div className='home-title'>
                ACEPOINTE
            </div>
            <div className='home-content'> 
            Merging creativity and technology to grow your brand in a digital world. 
            </div>
            <BottomNav name={'home'} url={'#about'} page={'ABOUT US'} />
        </div>
    ) 
}

export default Home;