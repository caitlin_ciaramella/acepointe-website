import React, {useEffect} from 'react';
import './About.css'
import BottomNav from '../PageFeatures/BottomNav';
import Particles from "react-particles-js";


function About() {
    return (
        <div className='about-container' id='about' >
            <div className='about-us-title'>
                Hey, we're AcePointe. 
            </div>
            <div className='mission-section'>
                <div className='mission-content'>
                   <p>
                        We’re a design and application development agency in Phoenix, AZ 
                        that specializes in web design, app development, and consulting for 
                        small businesses, startups and emerging brands.
                    </p>
                    <br />
                    <p>
                        From dynamic web designs to cutting-edge applications, we create 
                        custom solutions that not only tell your story but engages your 
                        users in outstanding  digital experiences. And our small team of 
                        highly dedicated and skilled professionals partner with you each 
                        step of the way!
                    </p>
                </div>
            </div>
            <div className='about-us-subtitle'>
                    Nice to Meet you.
            </div>
            <BottomNav name={'about'} url={'#services'} page={'SERVICES'} />
            
        </div> 
    )
}

export default About;